package ru.t1.chernysheva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.service.IConnectionService;

public abstract class AbstractDtoService {

    @NotNull
    protected IConnectionService connectionService;

}
