package ru.t1.chernysheva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.chernysheva.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;

public final class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<UserDTO> getEntityClass() {
        return UserDTO.class;
    }

    @Override
    @Nullable
    public UserDTO findByLogin(@NotNull String login) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.login = :login";
        return entityManager.createQuery(query, getEntityClass()).setParameter("login", login).getResultStream().findFirst().orElse(null);
    }

    @Override
    @Nullable
    public UserDTO findByEmail(@NotNull String email) {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.email = :email";
        return entityManager.createQuery(query, getEntityClass()).setParameter("email", email).getResultStream().findFirst().orElse(null);
    }

    @Override
    @NotNull
    public Boolean isLoginExist(@NotNull String login) {
        return findByLogin(login) != null;
    }

    @Override
    @NotNull
    public Boolean isEmailExist(@NotNull String email) {
        return findByEmail(email) != null;
    }

}

