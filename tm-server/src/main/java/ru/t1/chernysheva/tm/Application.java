package ru.t1.chernysheva.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.component.Bootstrap;
import ru.t1.chernysheva.tm.service.ConnectionService;

import java.sql.Connection;

public final class Application {

    public static void main(@Nullable final String... args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
