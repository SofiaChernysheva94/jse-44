package ru.t1.chernysheva.tm.api.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.validation.constraints.NotNull;
import java.sql.Connection;

public interface IConnectionService {

    EntityManagerFactory getEMFactory();

    EntityManager getEntityManager();

}
