package ru.t1.chernysheva.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.repository.model.IProjectRepository;
import ru.t1.chernysheva.tm.api.repository.model.IUserRepository;
import ru.t1.chernysheva.tm.api.service.IConnectionService;
import ru.t1.chernysheva.tm.api.service.model.IProjectService;
import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chernysheva.tm.exception.field.*;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.repository.model.ProjectRepository;
import ru.t1.chernysheva.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectService extends AbstractService implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull List<Project> result;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            result = repository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        boolean result;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            result = repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable Project result;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            result = repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable Project result;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            result = repository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable Project project;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            project = repository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(userId, project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull List<Project> result;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            result = repository.findAll(userId, sort);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final Project project;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            if (status == null) throw new StatusEmptyException();
            project.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final Project project;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            project = repository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            if (status == null) throw new StatusEmptyException();
            project.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        if (description != null && !description.isEmpty())
            project.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @Nullable final User user = userRepository.findOneById(userId);
            project.setUser(user);
            entityManager.getTransaction().begin();
            repository.add(userId, project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final Project project;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            project.setName(name);
            if (description != null && !description.isEmpty())
                project.setDescription(description);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final Project project;
        try {
            @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
            project = repository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            project.setName(name);
            if (description != null && !description.isEmpty())
                project.setDescription(description);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}

