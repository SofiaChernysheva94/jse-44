package ru.t1.chernysheva.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;
import ru.t1.chernysheva.tm.dto.response.AbstractResponse;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private ProjectDTO project;

    public AbstractProjectResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

}
