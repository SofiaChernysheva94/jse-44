package ru.t1.chernysheva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public class UserChangePasswordResponse extends AbstractUserResponse {

    private UserDTO user;

    public UserChangePasswordResponse(UserDTO user) {
        this.user = user;
    }

}
