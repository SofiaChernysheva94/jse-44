package ru.t1.chernysheva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_task", schema = "tm_test")
public final class TaskDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

    @Basic
    @Column(name = "userId")
    private String userId;

    @Basic
    @Column(name = "description")
    private String description;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Basic
    @Column(name = "project_id")
    private String projectId;

}
