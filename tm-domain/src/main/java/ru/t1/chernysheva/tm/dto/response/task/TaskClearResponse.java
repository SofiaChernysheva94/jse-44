package ru.t1.chernysheva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.response.user.AbstractUserResponse;

@Getter
@Setter
@NoArgsConstructor
public class TaskClearResponse extends AbstractUserResponse {
}
